from random import randint

def setup():
    background(255)
    size(680, 616)
    noStroke()
    check_all_cases()

def check_all_cases():
    cases = (((100, 10, 10, 150, 200, 150), (100, 80, 50, 180, 150, 180), 65, 200), \
             ((330, 10, 230, 150, 430, 150), (270, 20, 220, 120, 320, 120), 295, 200), \ 
             ((560, 10, 460, 150, 660, 150), (620, 20, 570, 120, 670, 120), 526, 200), \
             ((100, 210, 10, 350, 200, 350), (100, 353, 50, 380, 150, 380), 65, 400), \
             ((330, 210, 230, 350, 430, 350), (270, 220, 220, 320, 277, 280), 295, 400), \
             ((560, 210, 460, 350, 660, 350), (620, 220, 614, 280, 670, 320), 526, 400), \
             ((100, 410, 10, 550, 200, 550), (100, 460, 50, 530, 150, 530), 65, 600), \
             ((330, 410, 230, 550, 430, 550), (330, 550, 250, 570, 410, 570), 295, 600), \
             ((560, 410, 460, 550, 660, 550), (560, 570, 510, 550, 610, 550), 526, 600))
    for case in cases:
        fill(*[randint(0, 255) for i in range(3)])
        triangle(*case[0])
        fill(*[randint(0, 255) for i in range(3)])
        triangle(*case[1])
        fill(0)
        text("Case {}: {}".format(str(cases.index(case) + 1), str(check_overlap(case[0], case[1]))), case[2], case[3])

def check_overlap(tri_1, tri_2):
    x1_1, y1_1, x2_1, y2_1, x3_1, y3_1 = tri_1
    x1_2, y1_2, x2_2, y2_2, x3_2, y3_2 = tri_2
    lines_intersect = False
    for line_i in (((x1_1, y1_1), (x2_1, y2_1)), ((x1_1, y1_1), (x3_1, y3_1)), ((x2_1, y2_1), (x3_1, y3_1))):
        for line_j in (((x1_2, y1_2), (x2_2, y2_2)), ((x1_2, y1_2), (x3_2, y3_2)), ((x2_2, y2_2), (x3_2, y3_2))):
            if check_lines_intersect(line_i, line_j):
                lines_intersect = True
    if lines_intersect:
        return True
    else:
        larger_tri = tri_1
        smaller_tri = tri_2
        if shoelace(*tri_2) > shoelace(*tri_1):
            larger_tri, smaller_tri = smaller_tri, larger_tri
        x1_1, y1_1, x2_1, y2_1, x3_1, y3_1 = smaller_tri
        x1_2, y1_2, x2_2, y2_2, x3_2, y3_2 = larger_tri
        sub_tris_areas = []
        for pts_i in (((x1_2, y1_2), (x2_2, y2_2)), ((x1_2, y1_2), (x3_2, y3_2)), ((x2_2, y2_2), (x3_2, y3_2))):
            sub_tri_area = shoelace(pts_i[0][0], pts_i[0][1], pts_i[1][0], pts_i[1][1], x1_1, y1_1)
            sub_tris_areas.append(sub_tri_area)
        if round(sum(sub_tris_areas), 1) == round(shoelace(*larger_tri), 1):
            return True
    return False

def check_lines_intersect(line_1, line_2):
    slope_1, y_int_1 = slope_intercept(*line_1)
    slope_2, y_int_2 = slope_intercept(*line_2)
    # Check vertical lines
    # From next line to line before next comment, treat y_int as x_int
    if slope_1 == None and slope_2 == None:
        if y_int_1 == y_int_2:
            return True
        else:
            return False
    elif slope_1 == None:
        return y_int_1 > min(line_2[0][0], line_2[1][0]) and y_int_1 < max(line_2[0][0], line_2[1][0])
    elif slope_2 == None:
        return y_int_2 > min(line_1[0][0], line_1[1][0]) and y_int_2 < max(line_1[0][0], line_1[1][0])
    # Check equal slopes
    if slope_1 == slope_2:
        if y_int_1 == y_int_2:
            return True
        else:
            return False
    intersect_x = (y_int_2 - y_int_1) / (slope_1 - slope_2)
    intersect_y = slope_1 * intersect_x + y_int_1
    intersect_coord = (intersect_x, intersect_y)
    on_line_1 = True if round(pythagorean(intersect_coord, line_1[0]) + pythagorean(intersect_coord, line_1[1]), 1) == round(pythagorean(*line_1), 1) else False
    on_line_2 = True if round(pythagorean(intersect_coord, line_2[0]) + pythagorean(intersect_coord, line_2[1]), 1) == round(pythagorean(*line_2), 1) else False
    if on_line_1 and on_line_2:
        return True
    return False

def pythagorean(pt_1, pt_2):
    return sqrt((pt_1[0] - pt_2[0])**2 + (pt_1[1] - pt_2[1])**2)

def slope_intercept(pt_1, pt_2):
    m = get_slope(pt_1, pt_2)
    if m == None:
        return None, pt_1[0]
    b = pt_1[1] - m * pt_1[0]
    return m, b

def get_slope(pt_1, pt_2):
    if pt_1[0] - pt_2[0] == 0:
        return None
    return float((pt_1[1] - pt_2[1])) / float((pt_1[0] - pt_2[0]))

def shoelace(x1, y1, x2, y2, x3, y3):
    return 0.5 * abs(x1 * y2 + x2 * y3 + x3 * y1 - x2 * y1 - x3 * y2 - x1 * y3)
